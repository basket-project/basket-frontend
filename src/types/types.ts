export type ProductType = {
  id?: number;
  image: string;
  name: string;
  stock: number;
  price: number;
  description?: string;
};

export type BasketProductType = {
  product: ProductType;
  quantityBooked: number;
};
