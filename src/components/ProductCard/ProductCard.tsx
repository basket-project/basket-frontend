import { ProductType } from "../../types/types";
import useProductLogique from "../../utils/productHook";

const Card = ({ image, name, price, stock }: ProductType) => {
  const {
    stockQuantity,
    productInBasket,
    handleAddProductInBasket,
    handleRemoveProductInBasket,
  } = useProductLogique({ image, name, price, stock });
  return (
    <div className="shadow-xl card w-80 bg-base-100">
      <figure>
        <img src={image} alt="image-card" className="w-full h-48" />
      </figure>
      <div className="card-body">
        <h2 className="card-title">{name}</h2>
        <div>
          <span className="font-semibold">Quantité en stock: </span>
          <span>{stockQuantity}</span>
        </div>

        <div className="items-center justify-between card-actions">
          <p className="text-2xl font-semibold text-primary-bittersweet">
            {price}€
          </p>

          {productInBasket === 0 ? (
            <button
              className="btn btn-primary"
              onClick={handleAddProductInBasket}
            >
              <img
                src="/icons/add-to-cart.svg"
                alt="add to card"
                className="w-7"
              />
            </button>
          ) : (
            <div className="flex items-center justify-between space-x-1">
              <button
                className="text-xl btn btn-circle"
                onClick={handleRemoveProductInBasket}
              >
                -
              </button>

              <kbd className="kbd">{productInBasket}</kbd>

              <button
                className="text-xl btn btn-circle"
                onClick={handleAddProductInBasket}
              >
                +
              </button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Card;
