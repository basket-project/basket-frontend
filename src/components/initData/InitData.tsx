import { useEffect } from "react";
import { setLocalStorage } from "../../utils/localStorageHook";
import { getProducts } from "../../services/product";

const InitData = () => {
  useEffect(() => {
    getProducts().then((res) => {
      if (!res) return;
      setLocalStorage("products", JSON.stringify(res));
    });
  }, []);

  return <></>;
};

export default InitData;
