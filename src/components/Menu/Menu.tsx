import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import useLocalStorage from "../../utils/localStorageHook";
import { BasketProductType } from "../../types/types";

const Menu = () => {
  const [productsInBasketNumber, setProductsInBasketNumber] = useState(0);
  const allProductsInBasket = useLocalStorage("allProductsInBasket");

  useEffect(() => {
    const allProducts = allProductsInBasket
      ? JSON.parse(allProductsInBasket)
      : [];
    const totalQuantityBooked = allProducts.reduce(
      (acc: number, item: BasketProductType) => acc + item.quantityBooked,
      0
    );
    setProductsInBasketNumber(totalQuantityBooked);
  }, [allProductsInBasket]);
  return (
    <section className="fixed z-50 flex justify-center w-full px-5 top-5">
      <div className="flex items-center justify-between w-full h-16 max-w-5xl px-4 shadow-xl bg-base-200 rounded-3xl">
        <details className="dropdown">
          <summary className="m-1 btn">
            <img src="/icons/menu.svg" alt="menu-icon" className="w-7 h-7" />
          </summary>
          <ul className="p-2 shadow menu dropdown-content z-[1] bg-base-100 rounded-box w-52">
            <li>
              <Link to="/">Products</Link>
            </li>
            <li>
              <Link to="/cart">Cart</Link>
            </li>
          </ul>
        </details>

        <div className="flex items-center">
          <img
            src="/icons/market-stall.svg"
            alt="menu-icon"
            height={30}
            width={30}
          />
          <span className="text-2xl">
            <Link to="/">My Market</Link>
          </span>
        </div>

        <div className="relative">
          <div className="absolute w-6 h-6 text-white rounded-full bg-primary-bittersweet bottom-4 left-3">
            <p className="font-bold text-center text-white">
              {productsInBasketNumber}
            </p>
          </div>

          <Link to="/cart">
            <img
              src="/icons/shopping-cart.svg"
              alt="menu-icon"
              height={30}
              width={30}
            />
          </Link>
        </div>
      </div>
    </section>
  );
};

export default Menu;
