import { BasketProductType } from "../../types/types";
import useProductLogique from "../../utils/productHook";

interface IShoppingCard {
  productItem: BasketProductType;
}

const ShoppingCard = ({ productItem }: IShoppingCard) => {
  const {
    handleAddProductInBasket,
    handleRemoveProductInBasket,
    handleRemoveInCart,
  } = useProductLogique(productItem.product);
  return (
    <div className="shadow-xl card md:card-side bg-base-100">
      <figure className="h-auto w-80 md:w-96">
        <img
          src={productItem.product.image}
          alt="image-products"
          className="w-full md:h-full"
        />
      </figure>
      <div className="card-body">
        <div className="space-x-4">
          <span>Nom du produit:</span>
          <span className="font-bold">{productItem.product.name}</span>
        </div>

        <div className="space-x-4">
          <span>Quantité en stock:</span>
          <span className="font-bold">{productItem.product.stock}</span>
        </div>

        <div className="space-x-4">
          <span>Prix:</span>
          <span className="font-bold">{productItem.product.price}€</span>
        </div>

        <div>
          <p className="font-bold">Dans le panier:</p>

          <div className="flex items-center justify-between">
            <button
              className="text-xl btn btn-circle"
              onClick={handleRemoveProductInBasket}
            >
              -
            </button>

            <kbd className="kbd">{productItem.quantityBooked}</kbd>

            <button
              className="text-xl btn btn-circle"
              onClick={handleAddProductInBasket}
            >
              +
            </button>
          </div>
        </div>

        <div className="space-x-4">
          <span className="font-bold">A payer:</span>
          <kbd className="kbd">
            {(productItem.product.price * productItem.quantityBooked).toFixed(
              2
            )}
            €
          </kbd>
        </div>

        <div className="justify-center my-4 card-actions md:justify-end">
          <button className="btn btn-error" onClick={handleRemoveInCart}>
            <img
              src="/icons/trash-icon.svg"
              width={30}
              height={30}
              alt="trash"
            />
            Enlever
          </button>
        </div>
      </div>
    </div>
  );
};

export default ShoppingCard;
