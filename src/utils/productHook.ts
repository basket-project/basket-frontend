import { useState } from "react";
import useLocalStorage, {
  setLocalStorage,
  removeLocalStorage,
} from "./localStorageHook";
import { BasketProductType, ProductType } from "../types/types";

const useProductLogique = ({ image, name, price, stock }: ProductType) => {
  const [productInBasket, setProductInBasket] = useState(0);
  const [stockQuantity, setStockQuantity] = useState(stock);

  const allProductsInBasketFromStorage = useLocalStorage("allProductsInBasket");
  const products = useLocalStorage("products");

  const removeProductCart = () => {
    const TIMEOUT = 24 * 60 * 1000;
    setTimeout(() => {
      handleRemoveInCart();
      removeLocalStorage("allProductsInBasket");
    }, TIMEOUT);
  };

  const handleAddProductInBasket = () => {
    if (stock === 0) return;
    removeProductCart();
    setProductInBasket((prev) => prev + 1);
    setStockQuantity((prev: number) => prev - 1);

    const newProductAdded = {
      product: { image, name, price, stock: stock - 1 },
      quantityBooked: productInBasket + 1,
    };

    const allProducts =
      allProductsInBasketFromStorage === null
        ? []
        : JSON.parse(allProductsInBasketFromStorage);

    if (
      allProducts.length !== 0 &&
      allProducts.find((p: BasketProductType) => p.product.name === name)
    ) {
      const updatedProductsInBasket = allProducts.map(
        (p: BasketProductType) => {
          if (p.product.name === name) {
            p.product.stock -= 1;
            p.quantityBooked += 1;
          }
          return p;
        }
      );

      setLocalStorage(
        "allProductsInBasket",
        JSON.stringify(updatedProductsInBasket)
      );
    } else {
      setLocalStorage(
        "allProductsInBasket",
        JSON.stringify([
          ...allProducts.filter(
            (it: BasketProductType) => it.product.name !== name
          ),
          newProductAdded,
        ])
      );
    }

    if (products === null) return;

    const prodTab = JSON.parse(products);

    const updatedProducts = prodTab.map((p: ProductType) => {
      if (p.name === name) return { ...p, stock: p.stock - 1 };
      return p;
    });

    setLocalStorage("products", JSON.stringify(updatedProducts));
  };

  const handleRemoveProductInBasket = () => {
    removeProductCart();
    setProductInBasket((prev) => prev - 1);
    setStockQuantity((prev: number) => prev + 1);

    const allProducts: BasketProductType[] =
      allProductsInBasketFromStorage === null
        ? []
        : JSON.parse(allProductsInBasketFromStorage);

    if (allProducts.length === 0) return;

    if (allProducts.find((p: BasketProductType) => p.product.name === name)) {
      const updatedProductsInBasket = allProducts
        .map((p: BasketProductType) => {
          if (p.product.name === name) {
            p.product.stock += 1;
            p.quantityBooked -= 1;
          }
          return p;
        })
        .filter((p) => p.quantityBooked !== 0);

      setLocalStorage(
        "allProductsInBasket",
        JSON.stringify(updatedProductsInBasket)
      );

      if (products === null) return;

      const updatedProducts = JSON.parse(products).map((p: ProductType) => {
        if (p.name === name) return { ...p, stock: p.stock + 1 };
        return p;
      });

      setLocalStorage("products", JSON.stringify(updatedProducts));
    }
  };

  const handleRemoveInCart = () => {
    if (products === null) return;
    if (allProductsInBasketFromStorage === null) return;
    const productinStorage = JSON.parse(allProductsInBasketFromStorage);
    const productInCartQuantity: number | undefined = productinStorage.find(
      (p: BasketProductType) => p.product.name === name
    )?.quantityBooked;
    const updatedProducts = JSON.parse(products).map((p: ProductType) => {
      if (p.name === name) {
        p.stock = p.stock + (productInCartQuantity || 0);
      }
      return p;
    });
    setLocalStorage("products", JSON.stringify(updatedProducts));
    setLocalStorage(
      "allProductsInBasket",
      JSON.stringify(
        productinStorage.filter(
          (p: BasketProductType) => p.product.name !== name
        )
      )
    );
  };

  return {
    stockQuantity,
    productInBasket,
    handleAddProductInBasket,
    handleRemoveProductInBasket,
    handleRemoveInCart,
  };
};

export default useProductLogique;
