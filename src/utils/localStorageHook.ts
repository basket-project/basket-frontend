import { useState, useEffect } from "react";

export function setLocalStorage(key: string, value: string): void {
  localStorage.setItem(key, value);
  const event = new Event("localStorageSet");
  window.dispatchEvent(event);
}

export function removeLocalStorage(key: string): void {
  localStorage.removeItem(key);
  const event = new Event("localStorageRemove");
  window.dispatchEvent(event);
}

function useLocalStorage(key: string) {
  const [storedValue, setStoredValue] = useState(() => {
    return localStorage.getItem(key);
  });

  useEffect(() => {
    const handleStorageChange = () => {
      setStoredValue(localStorage.getItem(key));
    };

    window.addEventListener("localStorageSet", handleStorageChange);
    window.addEventListener("localStorageRemove", handleStorageChange);

    return () => {
      window.removeEventListener("localStorageSet", handleStorageChange);
      window.removeEventListener("localStorageRemove", handleStorageChange);
    };
  }, [key]);

  return storedValue;
}

export default useLocalStorage;
