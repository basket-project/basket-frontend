import ProductCard from "../../components/ProductCard";
import useLocalStorage from "../../utils/localStorageHook";
import { ProductType } from "../../types/types";

function Products() {
  const productsFromLocalStorage = useLocalStorage("products");
  const products: ProductType[] =
    productsFromLocalStorage !== null
      ? JSON.parse(productsFromLocalStorage)
      : [];

  return (
    <main className="flex justify-center min-h-screen">
      <section className="flex flex-col items-center justify-between w-full max-w-5xl p-24 space-y-4 lg:flex-row lg:flex-wrap">
        {products.length > 0 &&
          products.map((product) => (
            <ProductCard
              image={product.image}
              name={product.name}
              price={product.price}
              stock={product.stock}
              key={`product-` + product.id}
            />
          ))}
      </section>
    </main>
  );
}

export default Products;
