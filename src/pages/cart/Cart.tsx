import React from "react";
import { Link } from "react-router-dom";
import ShoppingCard from "../../components/ShoppingCard";
import useLocalStorage from "../../utils/localStorageHook";
import { BasketProductType } from "../../types/types";

function Cart() {
  const productsFromStorage = useLocalStorage("allProductsInBasket");
  const productInCart: BasketProductType[] = !productsFromStorage
    ? []
    : JSON.parse(productsFromStorage);

  const allProductsInBasketFromStorage = useLocalStorage("allProductsInBasket");

  const totalProductsPrice = () => {
    if (allProductsInBasketFromStorage === null) return 0;
    const productsData: BasketProductType[] = JSON.parse(
      allProductsInBasketFromStorage
    );
    if (productsData.length === 0) return 0;

    return productsData
      .reduce(
        (acc: number, it: BasketProductType) =>
          acc + it.product.price * it.quantityBooked,
        0
      )
      .toFixed(2);
  };

  const handlecloseModal = () => {
    const modal = document.getElementById(
      "my_modal"
    ) as HTMLDialogElement | null;
    if (modal) modal.showModal();
  };

  return (
    <main className="flex flex-col items-center min-h-screen mt-24">
      <h1 className="my-5 text-4xl font-semibold text-primary-dark-cyan">
        Shopping cart
      </h1>

      {productInCart.length === 0 && (
        <section className="spy-5">
          <img
            src="/icons/empty-cart.svg"
            alt="empty cart"
            width={500}
            height={500}
            className="w-52"
          />

          <p>Votre panier est encore vide.</p>
          <Link
            to="/"
            className="block text-center text-blue-500 underline underline-offset-2"
          >
            Voir les produits
          </Link>
        </section>
      )}

      <div className="space-y-4">
        {productInCart.length !== 0 &&
          productInCart.map((p: BasketProductType, index: number) => (
            <ShoppingCard
              key={p.product.name + index}
              productItem={{
                product: {
                  image: p.product.image,
                  name: p.product.name,
                  stock: p.product.stock,
                  price: p.product.price,
                },
                quantityBooked: p.quantityBooked,
              }}
            />
          ))}
      </div>

      {productInCart.length !== 0 && (
        <div className="my-5 card w-96 bg-primary-ivory">
          <div className="card-body">
            <p className="font-bold text-primary-dark-cyan">Total: </p>
            <p className="text-2xl font-semibold">{totalProductsPrice()}€</p>
            <div className="justify-end card-actions">
              <button
                className="text-white btn btn-primary"
                onClick={handlecloseModal}
              >
                Proceder au payement
              </button>
            </div>
          </div>
        </div>
      )}

      <dialog id="my_modal" className="modal">
        <div className="modal-box">
          <h3 className="text-lg font-bold text-center">My Market</h3>

          <div className="space-y-4 flex items-center flex-col w-[464px] my-5">
            <label className="flex items-center gap-2 input input-bordered w-80">
              <input type="text" className="grow" placeholder="Email" />
            </label>

            <label className="flex items-center gap-2 input input-bordered w-80">
              <input
                type="number"
                className="grow"
                placeholder="Numero de carte"
              />
            </label>

            <div className="flex justify-between space-x-6">
              <label className="flex items-center gap-2 input input-bordered">
                <input type="text" className="grow w-28" placeholder="MM/YY" />
              </label>

              <label className="flex items-center gap-2 input input-bordered">
                <input type="number" className="grow w-28" placeholder="CVC" />
              </label>
            </div>

            <button className="text-white btn btn-primary">
              Payer {totalProductsPrice()}€
            </button>
          </div>
        </div>
        <form method="dialog" className="modal-backdrop">
          <button>close</button>
        </form>
      </dialog>
    </main>
  );
}

export default Cart;
