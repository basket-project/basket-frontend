import { API_BASE_URL } from "../env";
export const getProducts = async () => {
  try {
    const req = `${API_BASE_URL}/products`;
    const res = await fetch(req);
    return await res.json();
  } catch (err) {
    console.log("error => 💥💥💥", err);
  }
};
