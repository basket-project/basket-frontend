import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Menu from "./components/Menu";
import Products from "./pages/products";
import Cart from "./pages/cart";
import InitData from "./components/initData/InitData";

const App = () => {
  return (
    <Router>
      <InitData />
      <Menu />
      <Routes>
        <Route path="/" element={<Products />} />
        <Route path="/products" element={<Products />} />
        <Route path="/cart" element={<Cart />} />
      </Routes>
    </Router>
  );
};

export default App;
