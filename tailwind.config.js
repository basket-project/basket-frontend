/** @type {import('tailwindcss').Config} */
module.exports = {
  daisyui: {
    themes: ["light"],
  },
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        primary: {
          ivory: "#F5FBEF",
          "black-bean": "#230007",
          "dark-cyan": "#588B8B",
          bittersweet: "#FF715B",
        },
      },
    },
  },
  plugins: [require("daisyui")],
};
