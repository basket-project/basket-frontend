# # Carrefour Java kata

## Consignes

Les instructions suivantes vous permettront d'obtenir une copie du projet opérationnel en local à des fins de développement et de test.

## Fonctionnalités

> - Afficher la liste des produits disponibles(avec stock disponible, prix...).
> - Ajouter, modifier ou retirer des produits dans un panier.
> - Retirez des prodtuits du panier si la date dépasse du 24h

### Technologies utilisées:

- front-end: reactjs, tailwindcss
- git

## Configuration

> - Cloner le projet en ssh ou http
> - Entrer dans le dossier racine de l'application `basket-frontend`

```
cd basket-frontend
```

> - Installer les packages en executant

```
npm i
```

> - Lancer le projet

```
npm start
```

> - Ouvrir le navigateur sur `http://localhost:3000/`

## Auteur

**Iangolana Riantsoa Ramelson** - [riantsoaramelson@gmail.com](mailto:riantsoaramelson@gmail.com)
